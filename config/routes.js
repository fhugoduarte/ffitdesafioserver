const express = require('express');
const easterEggsService =  require('../api/service/easterEggsService');
const userService = require('../api/service/usersService');

module.exports = function(server){

    //Rotas da API.
    const router = express.Router();

    //Método GET que retorna um JSON com a lista de todos os ovos de páscoa.
    router.get('/easter-eggs', function(req, res){
        let eggsList = easterEggsService.listar();
        if(eggsList.length == 0){
            res.status(404);
            res.send({error: 'Desculpe, não foi possível encontrar os ovos.'});
        }else{
            res.json(eggsList);
        }
    });

    /*Método GET que recebe um id como parametro e retorna o ovo de páscoa
        que possui esse id no formato JSON*/
    router.get('/easter-eggs/:id', function(req, res){
        let egg = easterEggsService.buscar(req.params.id);
        if(egg.length == 0){
            res.status(404);
            res.send({error: 'Desculpe, não foi possível encontrar este ovo.'});
        }else{
            res.json(egg[0]);
        }
    });

    /*Método POST que recebe no corpo da sua requisição um objeto JSON
        com os atributos email e senha. Retorna um JSON do usuário que
        possuir o email e senha igual ao passado no corpo da requisição.*/
    router.post('/auth', function(req, res){
        let user = userService.buscaUsuario(req.body);
        if(user.length == 0){
            res.status(404);
            res.send({error: 'Email/Senha inválidos!'});
        }else{
            res.json(user[0]);
        }
    });

    /*Método Delete que recebe no corpo da sua requisição um objeto JSON
        com os atributos email e senha. Retorna a permissão para realizar
        o logout do usuário.*/
        router.delete('/auth', function(req, res){
            let user = userService.buscaUsuario(req.body);
            if(user.length == 0){
                res.status(404);
                res.send({error: 'Usuário não encontrado!'});
            }else{
                res.send("OK");
            }
        });

    //Para acessar qualquer rota, antes é necessário colocar '/api/v1/{rota}'.
    server.use('/api/v1', router);

}