const bodyParser = require('body-parser');
const express = require('express');
const server = express();

//Define a porta que o servidor ficara escutando.
const port = 3003;

/*Transforma as respostas das requisições de formulário
    de modo que o backend possa trabalhar com esses valores.*/
server.use(bodyParser.urlencoded({extended: true}));

/*Qualquer requisição que retornar um JSON, será transformada
    em um objeto, podendo ser mais facilmente trabalhada no backend*/
server.use(bodyParser.json());

//Deixa o servidor escutando na porta definida. 
server.listen(port, function(){
    console.log('BACKEND rodando na porta: ' + port);
})

module.exports = server;