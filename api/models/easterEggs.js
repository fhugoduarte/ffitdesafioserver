//Lista de objetos que simulam ovos de páscoa cadastrados no sistema.

module.exports = [
    {
        id: 1,
        nome: 'KitKat White',
        marca: 'Nestlé',
        sabor: 'Chocolate Branco',
        peso: 313,
        preco: 20.00,
        imagem: 'https://images-americanas.b2w.io/produtos/01/00/sku/31971/9/31971919_1SZ.jpg'
    },{
        id: 2,
        nome: 'Alpino',
        marca: 'Nestlé',
        sabor: 'Chocolate ao leite',
        peso: 350,
        preco: 29.00,
        imagem: 'https://images-americanas.b2w.io/produtos/01/00/item/121761/3/121761332SZ.jpg'
    },{
        id: 3,
        nome: 'Alpino Dark',
        marca: 'Nestlé',
        sabor: 'Chocolate meio amargo',
        peso: 185,
        preco: 9.00,
        imagem: 'https://images-americanas.b2w.io/produtos/01/00/sku/31971/9/31971917_1SZ.jpg'
    },{
        id: 4,
        nome: 'Tortuguita',
        marca: 'Arcor',
        sabor: 'Chocolate Branco',
        peso: 150,
        preco: 15.00,
        imagem: 'https://static.carrefour.com.br/medias/sys_master/images/images/hc2/h5f/h00/h00/10708482261022.jpg'
    },{
        id: 5,
        nome: 'Negresco',
        marca: 'Garoto',
        sabor: 'Chocolate Branco',
        peso: 185,
        preco: 22.00,
        imagem: 'https://static.carrefour.com.br/medias/sys_master/images/images/hf3/h39/h00/h00/10708493139998.jpg'
    }
]