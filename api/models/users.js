//Lista de objetos que simulam usuários cadastrados no sistema. 

module.exports = [
    {
        id: 1,
        nome: 'João Pereira',
        email: 'joao@email.com',
        senha: '123',
        foto: 'https://themify.me/demo/themes/pinshop/files/2012/12/man-in-suit2.jpg'
    },{
        id: 2,
        nome: 'Maria Antônia',
        email: 'maria@email.com',
        senha: '123',
        foto: 'http://dwgyu36up6iuz.cloudfront.net/heru80fdn/image/upload/c_fill,d_placeholder_glamour.png,fl_progressive,g_face,h_450,q_80,w_800/v1477940820/glamour_woman-of-the-year-ashley-graham-sounds-off-on-shirtless-selfies-dirty-talk-and-the-thigh-gap.jpg'
    },{
        id: 3,
        nome: 'Antônio Pedro',
        email: 'antonio@email.com',
        senha: '123',
        foto: 'https://images.pexels.com/photos/428341/pexels-photo-428341.jpeg?auto=compress&cs=tinysrgb&h=350'
    }
]