const easterEggs = require('../models/easterEggs');

//Função que retorna a lista (easterEggs) completa.
exports.listar = function(){
    return easterEggs;
};

/*Função que recebe um id como parametro e usa esse dado para filtrar
    os objetos da lista (easterEggs), essa função retorna apenas os
    objetos que possuirem  o atributo id igual ao passado por parametro.*/
exports.buscar = function(id){
    return easterEggs.filter(function(egg){
        return egg.id == id;
    })
};