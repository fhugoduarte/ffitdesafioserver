const users = require('../models/users');

/*Função que recebe um objeto (userAuth) e filtra a lista (users)
    retornando apenas os usuários que possuirem os atributos email e senha
    iguais aos do objeto (userAuth) passado como parametro.*/
exports.buscaUsuario = function(userAuth){
    return users.filter(function(user){
        return user.email == userAuth.email && user.senha == userAuth.senha;
    })
}